// import ballerina/io;
import ballerina/graphql;

public type CovidDetails record {|
    readonly string region;
    int confirmed_cases = 0;
    int deaths = 0;
    int recovered = 0;
    int total = 0;
|};
//adding covid information to a table 
table<CovidDetails> key(region) covidEntriesTable = table [
        {region: "Khomas", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Oshana", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        {region: "Omusati", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Hardap", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Okavango East", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Okavango West", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Erongo", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        {region: "Otjozondjupa", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0},
        { region: "Zambezi", confirmed_cases: 0, deaths: 0, recovered: 0, total: 0}
    ];

public distinct service class CovidData {
    private final readonly & CovidDetails entryRecord;

    function init(CovidDetails entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }
    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get confirmed_cases() returns int? {
        return self.entryRecord.confirmed_cases;
    }

    resource function get deaths() returns int? {
        return self.entryRecord.deaths;
    }

    resource function get recovered() returns int? {
        return self.entryRecord.recovered;
    }

    resource function get total() returns int? {
        return self.entryRecord.total;
    }
}

public type Entry record {|
    int deaths = 0;
    int recovered = 0;
    int new_case = 0;
|};

service /graphql on new graphql:Listener(4000) {
    resource function get all() returns CovidData[] {
        CovidDetails[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string region) returns CovidData? {
        CovidDetails? covidEntry = covidEntriesTable[region];
        if covidEntry is CovidDetails {
            return new (covidEntry);
        }
        return;
    }

    remote function update(string region, Entry entry) returns CovidData {
        CovidDetails e = covidEntriesTable.get(region);

        if (entry.new_case > 0) {
            e.confirmed_cases += entry.new_case;
            e.total += entry.new_case;
        }

        if (entry.deaths > 0) {
            e.deaths += entry.deaths;
            e.total -= entry.deaths;
        }

        if (entry.recovered > 0) {
            e.recovered += entry.recovered;
            e.total -= entry.recovered;
        }

        return new CovidData(e);
    }
}
