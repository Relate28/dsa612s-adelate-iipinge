import ballerina/http;
import ballerina/io;

    // Creates a new client with the backend URL.
    final http:Client clientEndpoint = check new ("http://localhost:4000");
    json result = [];
public function main() returns error? {


    check LoginUI(clientEndpoint);
}

function LoginUI(http:Client clientEndpoint) returns error? {
    io:println("****************Welcome to Covid Data Center*****************");
    io:println("1. Get stats");
    io:println("2. Get by Region ");
    io:println("3. Update stats");
    io:println("****************************************");
    string choice = io:readln("Enter choice 1-3: ");



    if (choice == "1") {
        getAllRecords(); 
    } else if (choice == "2") {
      filterRecords();
    } else if (choice == "3") {
      updateRecords();
    }


    check LoginUI(clientEndpoint);

}

function getAllRecords() {
     
        result = checkpanic clientEndpoint->post("/graphql", {query: "{ all { region confirmed_cases  total } }"});
        io:println(result.toJsonString());
}

function filterRecords() {
        string region_code = io:readln("Enter Region : ");
        result = checkpanic clientEndpoint->post("/graphql", {query: "{ filter(region: \"" + region_code + "\" ) { region confirmed_cases recovered deaths } }"});
        io:println(result.toJsonString());
}

function updateRecords() {
           string region = io:readln("Enter Region : ");
        string cases = io:readln("Enter confirmed cases: ");
        string deaths = io:readln("Enter deaths: ");
        string recovery = io:readln("Enter recoveries: ");

        result = checkpanic clientEndpoint->post("/graphql", {query: "mutation{ update(region: \""+region+"\", entry: {deaths: "+deaths+", recovered: "+recovery+", new_case: "+cases+"}) { region confirmed_cases recovered deaths total } }"});
        io:println(result.toJsonString());
}